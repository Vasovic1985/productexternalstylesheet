﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Namirnice2.Startup))]
namespace Namirnice2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
