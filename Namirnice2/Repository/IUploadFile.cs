﻿using Namirnice2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Namirnice2.Repository
{
    interface IUploadFile
    {
        IQueryable<FileUploadModel> GetAll();
        FileUploadModel GetById(int id);       
        void Add(FileUploadModel fileUploadModel);
        void Update(FileUploadModel fileUploadModel);
        void Delete(FileUploadModel id);
    }
}
